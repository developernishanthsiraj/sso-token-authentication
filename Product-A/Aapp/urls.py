from django.urls import path
from .views import LoginView, CreateNoteView, FetchNotesView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('create-note/', CreateNoteView.as_view(), name='create_note'),
    path('fetch-notes/', FetchNotesView.as_view(), name='fetch_notes'),
]
