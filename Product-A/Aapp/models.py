from django.db import models

class Note(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    user_name = models.CharField(max_length=100)  # Assuming user_name is the username associated with the note

    def __str__(self):
        return self.title
