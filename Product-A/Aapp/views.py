from rest_framework.views import APIView
from rest_framework.response import Response
import requests
from .models import Note

class LoginView(APIView):
    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        # Make a request to the other Django server to obtain tokens
        response = requests.post('http://localhost:8000/api/login/', data={'username': username, 'password': password})
        
        if response.status_code == 200:
            tokens = response.json()
            # Extract username and tokens from the response
            access_token = tokens.get('token')

            # Set tokens in cookies
            response = Response({
                'token': access_token
            })
            # Set cookies here if required
            return response
        else:
            return Response({"error": "Invalid credentials"}, status=400)

    def decode_token(self, token):
        # Decode the token (in this case, it's not JWT but a custom token)
        try:
            decoded_token = {'user_id': token.split('-')[0]}  # Assuming the token format is "{user_id}-{random_string}"
            return decoded_token
        except Exception as e:
            return {}

class CreateNoteView(APIView):
    def post(self, request):
        title = request.data.get('title')
        description = request.data.get('description')
        token = request.data.get('token')

        if not title or not description or not token:
            return Response({"error": "Title, description, or access token is missing"}, status=400)

        # Check the validity of the access token
        response = requests.post('http://127.0.0.1:8000/api/check-token/', json={'token': token})
        
        if response.status_code == 200:
            # Extract user_id from the response
            user_name = response.json().get('user_name')

            # Create a new note instance with the provided data
            note = Note.objects.create(title=title, description=description, user_name=user_name)
            return Response({"message": "Note created successfully"}, status=201)
        else:
            return Response({"error": "token is invalid"}, status=401)

class FetchNotesView(APIView):
    def post(self, request):
        token = request.data.get('token')

        if not token:
            return Response({"error": "Access token is missing"}, status=400)

        # Send a request to check the access token
        response = requests.post('http://127.0.0.1:8000/api/check-token/', json={'token': token})
        
        if response.status_code == 200:
            # Check if the response contains 'user_id' key
            token_data = response.json()
            user_name = token_data.get('user_name')

            if user_name:
                # Fetch notes associated with the user_id
                notes = Note.objects.filter(user_name=user_name)
                serialized_notes = [{'title': note.title, 'description': note.description} for note in notes]
                return Response(serialized_notes, status=200)
            else:
                return Response({"error": "User not found in token data"}, status=400)
        else:
            return Response({"error": "Invalid token"}, status=401)
