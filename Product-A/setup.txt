
-------------SSO Credentials--------

http://127.0.0.1:8000/api/login/
input:
{
  "username": "admin",
  "password": "admin"
}

output:

{
  "token": "6e6d682726cf6b425f0a3ee372447ff5a95903a9"
}


http://127.0.0.1:8000/api/create-user/
input:
{
  "username": "nishanthsiraj",
  "password": "nishanth"
}

Authorization: Token 6e6d682726cf6b425f0a3ee372447ff5a95903a9

output:
{
  "username": "nishanthsiraj",
  "token": "29ec8fc88de35f5b798fc8363ed258b0baccdb05"
}


http://127.0.0.1:8000/api/check-token/
input:

{
  "token": "29ec8fc88de35f5b798fc8363ed258b0baccdb05"
}

output:

{
  "user_name": "nishanthsiraj"
}


-------------- PROJECT - A ----------------

http://127.0.0.1:8001/api/login/
input:
{
  "username": "nishanthsiraj",
  "password": "nishanth"
}

output:

{
  "token": "29ec8fc88de35f5b798fc8363ed258b0baccdb05"
}


http://127.0.0.1:8001/api/create-note/
input:
{
  "title":"hello world",
  "description": "hello user",
  "token": "29ec8fc88de35f5b798fc8363ed258b0baccdb05"
}

output:
{
  "message": "Note created successfully"
}


http://127.0.0.1:8001/api/fetch-note/
input:
{
  "token": "70fa212f7a1aa90c66d865c270c818eab4f64a64"
}

output:
[
  {
    "title": "hello world",
    "description": "hello user"
  }
]
