from django.urls import path
from .views import LoginView, LogoutView, UserCreateView, CheckTokenView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('create-user/', UserCreateView.as_view(), name='create_user'),
    path('check-token/', CheckTokenView.as_view(), name='check_token'),
]