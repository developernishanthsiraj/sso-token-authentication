# Create your models here.
import random
import string
from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    
    # Use unique related_name attributes to avoid clashes
    groups = models.ManyToManyField('Group', related_name='custom_user_set')
    user_permissions = models.ManyToManyField('Permission', related_name='custom_user_set')

    def save(self, *args, **kwargs):
        if not self.pk:
            # Generate a random 5-digit alphanumeric user ID
            super().save(*args, **kwargs)

class Group(models.Model):
    pass

class Permission(models.Model):
    pass