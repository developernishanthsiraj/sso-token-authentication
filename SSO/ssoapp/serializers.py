from rest_framework import serializers
from .models import CustomUser

class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = ['username', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        # Extract the password from the validated data
        password = validated_data.pop('password')

        # Create a new user object with the remaining data
        user = CustomUser(**validated_data)

        # Set and save the password using set_password() method to ensure it's properly hashed
        user.set_password(password)
        user.save()

        return user
