from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
# from rest_framework.authtoken.models import Token  <-- Remove this line
from rest_framework.authtoken.models import Token
from .serializers import UserCreateSerializer

class LoginView(APIView):
    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(request, username=username, password=password)

        if user:
            django_login(request, user)
            token, _ = Token.objects.get_or_create(user=user)

            # Set token in response headers
            response = Response({"token": token.key}, status=status.HTTP_200_OK)
            return response
        else:
            return Response({"error": "Invalid credentials"}, status=status.HTTP_400_BAD_REQUEST)


class LogoutView(APIView):
    def post(self, request):
        django_logout(request)
        return Response({"message": "Logout successful"}, status=status.HTTP_200_OK)


class UserCreateView(APIView):
    def post(self, request):
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            # Check if the user is an admin
            if request.user.is_superuser:
                user = serializer.save()

                # Generate token for the user
                token, _ = Token.objects.get_or_create(user=user)

                return Response({"username": user.username, "token": token.key,}, status=status.HTTP_201_CREATED)
            else:
                return Response({"error": "Only admin users can create new users"}, status=status.HTTP_403_FORBIDDEN)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class CheckTokenView(APIView):
    def post(self, request):
        token_key = request.data.get('token')

        if not token_key:
            return Response({"error": "Token is missing"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            token = Token.objects.get(key=token_key)
            username = token.user.username
            return Response({"user_name": username}, status=status.HTTP_200_OK)
        except Token.DoesNotExist:
            return Response({"error": "Token is invalid"}, status=status.HTTP_401_UNAUTHORIZED)
